from shops.actions import Actions
import sys

actions = Actions()
site_id = None
for i, param in enumerate(sys.argv):
    if i == 1:
        site_id = int(param)

actions.compare_products(site_id=site_id)