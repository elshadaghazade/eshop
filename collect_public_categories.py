# coding: utf-8

import requests as rq
import pymysql as mysql
from lxml import html
import re

conn = mysql.connect(host="localhost", user="root", password="root", database="eshops", charset="utf8")
strsql = """select id, name_en from public_categories"""
cur = conn.cursor()
cur.execute(strsql)
symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяüöğçşıəАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯÜÖĞÇŞIƏ",
           u"abvgdeejzijklmnoprstufhzcss_y_euauogcsieABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUAUOGCSIE")

tr = {ord(a):ord(b) for a, b in zip(*symbols)}

for id,name, in cur:
    cur2 = conn.cursor()
    name = name.translate(tr)
    name = re.sub(r"[^a-zA-Z0-9]+", "-", name)
    name = re.sub(r"(\-)+", "-", name)
    name = name.strip('-')
    strsql = """update public_categories set permalink_en = %(url)s where id = %(id)s"""
    cur2.execute(strsql, {
        "url": name.lower(),
        "id": id
    })
    conn.commit()
    cur2.close()
cur.close()
conn.close()