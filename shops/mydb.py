import pymysql as mysql

class MyDB:

    @staticmethod
    def get_conn():
        return mysql.connect(
                user="root",
                password="root",
                database="eshops",
                host="localhost",
                charset="utf8"
            )