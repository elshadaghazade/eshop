# coding: utf-8

from ... import parent_shop
from ... import mydb
import requests as rq
from lxml import html, etree
from StringIO import StringIO

class Shop(parent_shop.ParentShop):
    __conn = None

    def __init__(self):
        self.__conn = mydb.MyDB.get_conn()
        self.__lang = "az"

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def collect_categories(self):
        dom = self.get_dom(self.get_site()['url'])
        categories = dom.xpath('//*[@class="categories-wrapper"]/ul/li')
        cats = []
        for category in categories[:-1]:
            parent_id = 0
            category_name = category.xpath('./a//text()')
            category_name = category_name[0] if len(category_name) else None

            if not category_name:
                continue

            category_url = category.xpath('./a//@href')
            category_url = category_url[0] if len(category_url) else None
            category_url = category_url if category_url.startswith("http") else self.get_site()['url'] + category_url
            parent_id = self.insel_cat(parent_id=parent_id, name=category_name, url=category_url,lang='az')
            cat1 = {"name": category_name, "url": category_url, "subs": []}
            cats.append(cat1)

            categories2 = category.xpath('./ul/li')
            if not len(categories2):
                continue

            for category2 in categories2:
                category_name = category2.xpath('./a//text()')
                category_name = category_name[0] if len(category_name) else None

                if not category_name:
                    continue

                category_url = category2.xpath('./a//@href')
                category_url = category_url[0] if len(category_url) else None
                category_url = category_url if category_url.startswith("http") else self.get_site()['url'] + category_url
                parent_id = self.insel_cat(parent_id=parent_id, name=category_name, url=category_url,lang='az')
                cat2 = {"name": category_name, "url": category_url, "subs": []}
                cat1['subs'].append(cat2)

                categories3 = category2.xpath('./ul/li')
                if not len(categories3):
                    continue

                for category3 in categories3:
                    category_name = category3.xpath('./a//text()')
                    category_name = category_name[0] if len(category_name) else None

                    if not category_name:
                        continue

                    category_url = category3.xpath('./a//@href')
                    category_url = category_url[0] if len(category_url) else None
                    category_url = category_url if category_url.startswith("http") else self.get_site()['url'] + category_url
                    parent_id = self.insel_cat(parent_id=parent_id, name=category_name, url=category_url,lang='az')
                    cat3 = {"name": category_name, "url": category_url, "subs": []}
                    cat2['subs'].append(cat3)

                    categories4 = category3.xpath('./ul/li')
                    if not len(categories4):
                        continue

                    for category4 in categories4:
                        category_name = category4.xpath('./a//text()')
                        category_name = category_name[0] if len(category_name) else None

                        if not category_name:
                            continue

                        category_url = category4.xpath('./a//@href')
                        category_url = category_url[0] if len(category_url) else None
                        category_url = category_url if category_url.startswith("http") else self.get_site()['url'] + category_url
                        parent_id = self.insel_cat(parent_id=parent_id, name=category_name, url=category_url,lang='az')
                        cat4 = {"name": category_name, "url": category_url, "subs": []}
                        cat4['subs'].append(cat4)

    def insel_cat(self, **kwargs):
        """selects if exists or inserts new category"""
        cur = self.__conn.cursor()
        strsql = """select id from private_categories 
                    where url = %(url)s
                    and lang = %(lang)s
                 """
        res = cur.execute(strsql, {
            "url": kwargs.get("url"),
            "lang": kwargs.get("lang")
        })

        if res < 1:
            strsql = """insert into private_categories(parent_id,name,lang,url,site_id,create_tm,modify_tm)
                        values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()),unix_timestamp(now()))
                     """
            cur.execute(strsql, {
                "parent_id": kwargs.get("parent_id"),
                "name": kwargs.get("name"),
                "lang": kwargs.get("lang"),
                "url": kwargs.get("url"),
                "site_id": self.get_site()['id']
            })
            self.__conn.commit()
            id = cur.lastrowid
        else:
            for id, in cur:
                pass
        cur.close()
        return id


    def collect_products(self):
        """collects products from categories"""
        try:
            cur = self.__conn.cursor()
            strsql = """select id, url 
                        from private_categories 
                        where site_id = %(site_id)s
                        and modify_tm < unix_timestamp(now() - interval 1 day)
                     """
            res = cur.execute(strsql, {
                "site_id": self.get_site()['id']
            })

            if res < 1:
                return
        except Exception as err:
            print("Product collection error", err)
            return

        lang = "az"

        print("ofisait.com started collecting products")
        for category_id, category_url, in cur:
            # get products list
            try:
                links_list = self.get_product_links_from_category(category_url)
            except Exception as err:
                print("Get product links error:", err)
                continue

            for lst in links_list:
                for url in lst:
                    url = self.get_site()['url'] + url if not url.startswith('http') else url
                    # check if product exists
                    product_id = self.product_exists(url=url, lang=lang)
                    if product_id:
                        print("product exists ", url)
                        strsql = """update products set category_id = %(category_id)s where id = %(id)s"""
                        cur2 = self.__conn.cursor()
                        cur2.execute(strsql, {
                            "category_id": category_id,
                            "id": product_id
                        })
                        self.__conn.commit()
                        cur2.close()
                        continue

                    print("inserting product ", url)

                    try:
                        dom = self.get_dom(url)
                    except Exception as err:
                        print("DOM error:", err, url)
                        continue

                    try:
                        # get category id
                        category_id = self.save_category(
                            dom=dom,
                            lang=lang,
                            site_id=self.get_site()['id']
                        )
                    except Exception as err:
                        print("Category error:", err, url)
                        continue

                    try:
                        brand_id = self.save_brand(dom)
                    except Exception as err:
                        print("Brand error:", err)
                        brand_id = 0

                    try:
                        product_id = self.save_product(
                            dom=dom,
                            url=url,
                            category_id=category_id,
                            lang=lang,
                            brand_id=brand_id
                        )

                        print("Product inserted. ID: ", product_id)
                    except Exception as err:
                        print("Product save error:", err, url)
                        continue

                    try:
                        self.save_images(dom=dom,product_id=product_id)
                    except Exception as err:
                        print("Image error:", err, url)
            try:
                strsql = """update private_categories set modify_tm = unix_timestamp(now())
                            where id = %(id)s
                         """
                cur_cat = self.__conn.cursor()
                cur_cat.execute(strsql, {
                    "id": category_id
                })
                self.__conn.commit()
                cur_cat.close()
            except Exception as err:
                print("private category modify_tm update err:", err)

    def update_products(self):
        """update products"""
        curprd = self.__conn.cursor()

        strsql = """select id, url, lang from products 
                    where site_id = %(site_id)s
                    order by scan_tm asc limit 1000"""
        curprd.execute(strsql, {
            "site_id": self.get_site()['id']
        })
        for product_id, url, lang, in curprd:
            print("updating product ", url)

            try:
                dom = self.get_dom(url)
            except Exception as err:
                print("DOM error:", err, url)
                continue

            try:
                # get category id
                category_id = self.save_category(
                    dom=dom,
                    lang=lang,
                    site_id=self.get_site()['id']
                )
            except Exception as err:
                print("Category error:", err, url)
                continue

            try:
                brand_id = self.save_brand(dom)
            except Exception as err:
                print("Brand error:", err)
                brand_id = 0

            try:
                product_id = self.save_product(
                    dom=dom,
                    url=url,
                    category_id=category_id,
                    lang=lang,
                    brand_id=brand_id,
                    product_id=product_id
                )

                print("Product updated. ID: ", product_id)
            except Exception as err:
                print("Product save error:", err, url)
                continue

            try:
                self.save_images(dom=dom,product_id=product_id)
            except Exception as err:
                print("Image error:", err, url)
        curprd.close()

    def save_brand(self, dom):
        """saves brand"""

        brand = dom.xpath('//ul[@class="info"]/li/div[@class="item-info"]/a[@itemprop="name"]//text()')
        if not len(brand):
            return 0
        brand = brand[0].strip()

        cur = self.__conn.cursor()
        strsql = """select id from brands where name = %(name)s"""
        res = cur.execute(strsql, {
            "name": brand
        })
        if res > 0:
            for id, in cur:
                return id

        strsql = """insert into brands(name)values(%(name)s)"""
        cur.execute(strsql,{
            "name": brand
        })
        self.__conn.commit()
        return cur.lastrowid


    def get_product_links_from_category(self, url):
        """Collects product links from category page"""
        urls = [url + "&pg=" + str(pg) for pg in range(1,1000)]
        for url in urls:
            try:
                dom = self.get_dom(url)
            except Exception as err:
                continue
            links = dom.xpath('//*[@class="ProductPhoto"]/a//@href')
            if not len(links):
                break

            yield links
        #return links

    def save_images(self, **kwargs):
        """saves images"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        cur = self.__conn.cursor()

        images = set()
        imgs = dom.xpath('//img[@itemprop="image"]//@src')
        for img in imgs:
            img = self.get_site()['url'] + img if not img.startswith('http') else img
            images.add(img)

        for pos,img in enumerate(images):
            is_default = 1 if pos == 0 else 0

            strsql = """insert into product_images(url,product_id,is_default)
                        values(%(url)s,%(product_id)s,%(is_default)s)
                     """
            cur.execute(strsql, {
                "url": img,
                "product_id": kwargs.get("product_id"),
                "is_default": is_default
            })
            self.__conn.commit()
        cur.close()

    def save_product(self, **kwargs):
        """saves product"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        name = dom.xpath('//h1[@itemprop="name"]//text()')
        name = name[0].strip() if len(name) else None

        price = dom.xpath('//meta[@itemprop="price"]//text()')
        price = price[0].split(" ")[0] if len(price) else 0

        currency = dom.xpath('//meta[@itemprop="priceCurrency"]//@content')
        currency = currency[0].lower() if len(currency) else 'azn'

        order_url = kwargs.get("url")

        description = dom.xpath('//*[@class="text_product"]//text()')
        description = '\n'.join(description).strip() if len(description) else None

        product_id = kwargs.get("product_id")

        cur = self.__conn.cursor()


        if product_id:
            # update product
            strsql = """update products 
                        set category_id = %(category_id)s,
                        lang = %(lang)s,
                        name = %(name)s,
                        brand_id = %(brand_id)s,
                        price = %(price)s,
                        description = %(description)s,
                        currency = %(currency)s,
                        scan_tm = unix_timestamp(now())
                        where id = %(id)s
                        """
            cur.execute(strsql, {
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "brand_id": kwargs.get("brand_id"),
                "id": product_id
            })
        else:
            # insert product
            strsql = """insert into products(site_id,url,category_id,lang,name,brand_id,price,description,currency,order_url,create_tm,scan_tm)
                        values(%(site_id)s,%(url)s,%(category_id)s,%(lang)s,%(name)s,%(brand_id)s,%(price)s,%(description)s,%(currency)s,%(order_url)s,unix_timestamp(now()), unix_timestamp(now()))
                     """

            cur.execute(strsql, {
                "site_id": self.get_site()['id'],
                "url": kwargs.get("url"),
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "order_url": order_url,
                "brand_id": kwargs.get("brand_id")
            })
        self.__conn.commit()
        cur.close()
        return product_id if product_id else cur.lastrowid

    def get_tree(self,sitemap):
        """returns sitemap tree"""
        r = rq.get(sitemap[0])
        if r.status_code != 200:
            print("cannot get content of {}".format(self.get_site()['url']))
            return []

        return etree.iterparse(StringIO(r.content))

    def product_exists (self, url, lang):
        """checks if product exists"""
        cur = self.__conn.cursor()
        strsql = "select id from products where url = %(url)s and lang = %(lang)s"
        res = cur.execute(strsql, {"url":url, "lang": lang})
        cur.close()
        if res > 0:
            for id, in cur:
                return id
        else:
            return False


    def save_category(self, **kwargs):
        """insert/select category"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is incorrect")

        categories = dom.xpath('//*[@class="pr-nav"]//ul//a')
        if len(categories) < 1:
            raise Exception("Category was not found")

        category_id = 0
        for category in categories[1:]:
            category_name = category.xpath('.//text()')
            category_name = category_name[0].strip() if len(category_name) and category_name[0].strip() else None

            category_url = category.xpath('.//@href')
            category_url = self.get_site()['url'] +  category_url[0] if len(category_url) else None

            cur = self.__conn.cursor()

            strsql = "select id from private_categories where url=%(url)s and lang = %(lang)s"
            res = cur.execute(strsql, {
                'url': category_url,
                'lang': kwargs.get('lang')
            })

            if res < 1:
                strsql = """insert into private_categories(parent_id,name,lang,url,site_id,create_tm)
                            values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))
                         """
                cur.execute(strsql, {
                    'name': category_name,
                    'lang': kwargs.get('lang'),
                    'site_id': kwargs.get('site_id'),
                    "url": category_url,
                    "parent_id": category_id
                })
                self.__conn.commit()
                category_id = cur.lastrowid
            else:
                for category_id, in cur:
                    pass

            cur.close()
        return category_id


