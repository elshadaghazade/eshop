from mydb import MyDB as mydb

class Sites:
    __conn = None

    def __init__(self):
        self.__conn = mydb.get_conn()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def get_sites(self, id=None):
        cur = self.__conn.cursor()
        strsql = """select 
                    s.id, 
                    s.url, 
                    m.namespace
                    from sites s
                    inner join modules m on m.id = s.module_id
                    where if(%(id)s is not null, s.id=%(id)s, s.id is not null)
                    order by s.scan_tm asc"""
        cur.execute(strsql, {"id":id})
        sites = []
        for id, url, namespace, in cur:
            sites.append({
                'id': id,
                'url': url,
                'namespace': namespace
            })

        return sites
