from difflib import SequenceMatcher as sm
from shops.mydb import MyDB as mydb

class Comparator:
    __conn = None

    def __init__(self):
        self.__conn = mydb.get_conn()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def compare_products(self):
        strsql = """select id, name, lang, site_id 
                    from products
                    where compared = 0 
                    order by site_id desc"""
        cur1 = self.__conn.cursor()
        res = cur1.execute(strsql)
        if res < 1:
            return

        i = 0

        cur2 = self.__conn.cursor()
        for id, name, lang, site_id, in cur1:
            i += 1
            print(i, ". searching similarity for ", id, name, lang, site_id)
            strsql = """select id, 
                               name 
                        from products 
                        where not id = %(id)s 
                        and not site_id = %(site_id)s 
                        and lang = %(lang)s
                        and not id in (select product_left from product_similarity where product_right = %(id)s)
                        and not id in (select product_right from product_similarity where product_left = %(id)s)
                        """
            res = cur2.execute(strsql, {
                "id": id,
                "site_id": site_id,
                "lang": lang
            })

            if res < 1:
                continue

            cur3 = self.__conn.cursor()
            for id2, name2, in cur2:
                ratio = sm(None, name, name2).ratio()
                if ratio < 0.4:
                    continue

                print(ratio, name, name2)
                strsql = """insert into product_similarity(product_left,product_right,ratio)
                            values(%(product_left)s,%(product_right)s,%(ratio)s)
                         """
                try:
                    cur3.execute(strsql, {
                        "product_left": id,
                        "product_right": id2,
                        "ratio": ratio
                    })
                    self.__conn.commit()
                except Exception as err:
                    print err
            cur4 = self.__conn.cursor()
            strsql = "update products set compared = 1 where id = %(id)s"
            cur4.execute(strsql, {
                "id": id
            })
            self.__conn.commit()
            cur4.close()
            cur3.close()
        cur2.close()
        cur1.close()
