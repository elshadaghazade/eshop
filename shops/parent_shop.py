import pymysql as mysql
import requests as rq
from lxml import html

class ParentShop:
    __lang = "az"
    __is_actvie = 1
    __site = None

    def set_site(self, site):
        self.__site = site

    def get_site(self):
        return self.__site

    def get_dom(self, url):
        """get page dom"""
        r = rq.get(url)
        if r.status_code != 200:
            raise Exception("cannot get content of {}".format(url))

        return html.fromstring(r.content)