# coding: utf-8
from ... import parent_shop
from ... import mydb
import requests as rq
from lxml import html
import re

class Shop(parent_shop.ParentShop):
    __conn = None

    def __init__(self):
        self.__conn = mydb.MyDB.get_conn()
        self.__lang = "ru"

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def collect_categories(self):
        print("topshops.az categories started be collected...")
        languages = ['ru', 'az']

        for lang in languages:
            r = rq.get(self.get_site()['url'], cookies={'language': lang})
            if r.status_code != 200:
                print("cannot get content of {}".format(self.get_site()['url']))
                return

            dom = html.fromstring(r.content)
            menuleft = dom.xpath('//*[@class="menu-left"]//li[@class="sub"]')
            categories = []
            for cat in menuleft:
                a = cat.xpath('.//span//a')
                if len(a) != 1:
                    continue

                name = a[0].xpath('.//text()')[0]
                url = a[0].xpath('.//@href')[0]
                subcategories = []
                categories.append({
                    'name': name,
                    'url': url,
                    'lang': lang,
                    'site_id': self.get_site()['id'],
                    'subcategories': subcategories
                })

                for sub in cat.xpath('.//div[@class="panel"]//li'):
                    a = sub.xpath('.//a')
                    if len(a) != 1:
                        continue

                    name = a[0].xpath('.//text()')[0]
                    url = a[0].xpath('.//@href')[0]
                    subcategories.append({
                        'name': name,
                        'url': url,
                        'lang': self.__lang,
                        'site_id': self.get_site()['id'],
                        'subcategories': []
                    })

            cur = self.__conn.cursor()
            for cat in categories:
                print("insert ", cat['name'], cat['url'], cat['lang'])
                strsql = """insert into private_categories (parent_id,name,lang,url,site_id,create_tm) 
                            values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))"""

                try:
                    cur.execute(strsql, {
                        'parent_id': 0,
                        'lang': cat['lang'],
                        'url': cat['url'],
                        'name': cat['name'],
                        'site_id': cat['site_id']
                    })
                    self.__conn.commit()
                except Exception as err:
                    print(err)
                    continue
                lastrowid = cur.lastrowid

                for sub in cat['subcategories']:
                    print("insert ", sub['name'], sub['url'], sub['lang'])
                    strsql = """insert into private_categories (parent_id,name,lang,url,site_id,create_tm) 
                                values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))"""

                    try:
                        cur.execute(strsql, {
                            'parent_id': lastrowid,
                            'lang': sub['lang'],
                            'url': sub['url'],
                            'name': sub['name'],
                            'site_id': sub['site_id']
                        })
                        self.__conn.commit()
                    except Exception as err:
                        print(err)
            cur.close()


    def collect_products(self):
        strsql = "select url, lang from private_categories where site_id = %(site_id)s and parent_id = 0"
        cur = self.__conn.cursor()
        res = cur.execute(strsql, {"site_id":self.get_site()['id']})
        sum = 0
        for url, lang, in cur:
            r = rq.get(url, cookies={"language":"az"})
            if r.status_code != 200:
                print("cannot get content of {}".format(url))
                continue

            dom = html.fromstring(r.content)
            results = dom.xpath('//*[@class="results"]//text()')
            if not len(results):
                print("Pages count was not found")
                continue

            results = results[0].split(" ")
            url = url + "&limit=" + results[-4]
            r = rq.get(url, cookies={"language": "az"})
            if r.status_code != 200:
                print("cannot get content of {}".format(url))
                continue
            dom = html.fromstring(r.content)
            urls = dom.xpath('//*[@class="img-prod"]//a//@href')
            for url in urls:
                try:
                    url = re.sub(r"\?limit=[0-9]+$", "", url.strip())
                    self.save_product(url=url, lang=lang)
                except Exception as err:
                    print("Product save error:", err)
                    continue

    def update_products(self):

        curprd = self.__conn.cursor()

        strsql = """select id, url, lang from products 
                    where site_id = %(site_id)s
                    order by scan_tm asc limit 1000"""
        curprd.execute(strsql, {
            "site_id": self.get_site()['id']
        })
        for product_id, url, lang, in curprd:

            try:
                self.update_product(url=url, lang=lang, product_id=product_id)
            except Exception as err:
                print("Product save error:", err)
                continue
        curprd.close()

    def save_product(self, **kwargs):
        url = kwargs.get("url")
        print("inserting product", url)
        cur = self.__conn.cursor()
        strsql = "select id from products where url = %(url)s and lang = %(lang)s"
        res = cur.execute(strsql, {
            "url": url,
            "lang": kwargs.get("lang")
        })
        if res:
            raise Exception("Product exists")

        for lang in ["az", "ru"]:
            r = rq.get(url, cookies={"language":lang})
            if r.status_code != 200:
                raise Exception("Product page is not correct {}".format(url))

            dom = html.fromstring(r.content)
            breadcrump = dom.xpath('//*[@class="breadcrumb"]//a')
            if len(breadcrump) < 1:
                raise Exception("Breadcrumb error")

            product_name = breadcrump[-1].xpath('.//text()')[0]
            exists = dom.xpath('.//*[@class="description"]//text()')
            exists = exists[0] if len(exists) else ""
            in_stock = 0 if not exists.find(u"Mövcuddur") else 1
            price = dom.xpath('.//*[@class="price-b"]//text()')
            price = re.sub(r'[^0-9\.]+', '', price[0].strip()) if len(price) else None
            description = dom.xpath('//*[@id="tab-description"]//text()')
            description = description[0] if len(description) else None
            try:
                category_id = self.save_categories(breadcrumps=breadcrump[1:-1], lang=lang)
            except Exception as err:
                raise Exception("Product save error", err)
            strsql = """insert into products(site_id,url,category_id,lang,name,price,description,currency,order_url,in_stock,create_tm,scan_tm)
                        values(%(site_id)s,%(url)s,%(category_id)s,%(lang)s,%(name)s,%(price)s,%(description)s,%(currency)s,%(order_url)s,%(in_stock)s,unix_timestamp(now()),unix_timestamp(now()))
                     """

            res = cur.execute(strsql, {
                "site_id": self.get_site()['id'],
                "url": url,
                "category_id": category_id,
                "lang": lang,
                "name": product_name,
                "price": price,
                "description": description,
                "currency": "azn",
                "order_url": url,
                "in_stock": in_stock
            })

            if res < 1:
                raise Exception("Product could not be saved", url)
            product_id = cur.lastrowid

            self.save_images(dom=dom, product_id=product_id)

        cur.close()

    def update_product(self, **kwargs):
        url = kwargs.get("url")
        product_id = kwargs.get("product_id")
        lang = kwargs.get("lang")

        print("updating product", url)
        cur = self.__conn.cursor()

        r = rq.get(url, cookies={"language":lang})
        if r.status_code != 200:
            raise Exception("Product page is not correct {}".format(url))

        dom = html.fromstring(r.content)
        breadcrump = dom.xpath('//*[@class="breadcrumb"]//a')
        if len(breadcrump) < 1:
            raise Exception("Breadcrumb error")

        product_name = breadcrump[-1].xpath('.//text()')[0]
        exists = dom.xpath('.//*[@class="description"]//text()')
        exists = exists[0] if len(exists) else ""
        in_stock = 0 if not exists.find(u"Mövcuddur") else 1
        price = dom.xpath('.//*[@class="price-b"]//text()')
        price = re.sub(r'[^0-9\.]+', '', price[0].strip()) if len(price) else None
        description = dom.xpath('//*[@id="tab-description"]//text()')
        description = description[0] if len(description) else None
        category_id = self.save_categories(breadcrumps=breadcrump[1:-1], lang=lang)

        if product_id:
            # update product
            strsql = """update products 
                        set category_id = %(category_id)s,
                        lang = %(lang)s,
                        name = %(name)s,
                        price = %(price)s,
                        description = %(description)s,
                        scan_tm = unix_timestamp(now())
                        where id = %(id)s
                        """
            cur.execute(strsql, {
                "category_id": category_id,
                "lang": kwargs.get("lang"),
                "name": product_name,
                "price": price,
                "description": description,
                "id": product_id
            })
        else:
            strsql = """insert into products(site_id,url,category_id,lang,name,price,description,currency,order_url,in_stock,create_tm,scan_tm)
                            values(%(site_id)s,%(url)s,%(category_id)s,%(lang)s,%(name)s,%(price)s,%(description)s,%(currency)s,%(order_url)s,%(in_stock)s,unix_timestamp(now()),unix_timestamp(now()))
                         """
            cur.execute(strsql, {
                "site_id": self.get_site()['id'],
                "url": url,
                "category_id": category_id,
                "lang": lang,
                "name": product_name,
                "price": price,
                "description": description,
                "currency": "azn",
                "order_url": url,
                "in_stock": in_stock
            })
            product_id = cur.lastrowid

        self.save_images(dom=dom, product_id=product_id)

        cur.close()

    def save_images(self, **kwargs):
        """saves product images"""
        cur = self.__conn.cursor()
        images = set(kwargs.get("dom").xpath('//*[@class="MagicToolboxContainer"]//a//@href'))
        for pos, image in enumerate(images):
            is_default = 1 if pos == 0 else 0
            strsql = """insert into product_images(url,product_id,is_default)
                        values(%(url)s,%(product_id)s,%(is_default)s)
                     """
            cur.execute(strsql, {
                "url": image,
                "product_id": kwargs.get("product_id"),
                "is_default": is_default
            })
            self.__conn.commit()

    def save_categories(self, **kwargs):
        """saving categories from breadcrump and returns last child id"""
        cur = self.__conn.cursor()
        parent_id = 0
        for breadcrump in kwargs.get("breadcrumps"):
            category_name = breadcrump.xpath('.//text()')
            category_name = category_name[0] if len(category_name) else None

            category_url = breadcrump.xpath('.//@href');
            category_url = category_url[0] if len(category_url) else None

            strsql = "select id from private_categories where url = %(url)s and lang = %(lang)s"
            res = cur.execute(strsql, {
                "url": category_url,
                "lang": kwargs.get("lang")
            })

            if res < 1:
                strsql = """insert into private_categories(parent_id,name,lang,url,site_id,create_tm,modify_tm)
                            values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()),unix_timestamp(now()))
                         """
                cur.execute(strsql,{
                    "parent_id": parent_id,
                    "name": category_name,
                    "lang": kwargs.get("lang"),
                    "site_id": self.get_site()['id'],
                    "url": category_url
                })
                self.__conn.commit()

                parent_id = cur.lastrowid
            else:
                parent_id = [pid for pid, in cur][0]

        cur.close()
        return parent_id

