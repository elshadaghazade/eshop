# coding: utf-8

from ... import parent_shop
from ... import mydb
import requests as rq
from lxml import html, etree
from StringIO import StringIO

class Shop(parent_shop.ParentShop):
    __conn = None

    def __init__(self):
        self.__conn = mydb.MyDB.get_conn()
        self.__lang = "az"

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def collect_categories(self):
        print("shop.az categories started be collected...")
        sitemaps = [
            ['https://shop.az/sitemap-categories-az.xml','az'],
            ['https://shop.az/sitemap-categories-en.xml','en'],
            ['https://shop.az/sitemap-categories-ru.xml','ru']
        ]

        for sitemap in sitemaps:
            r = rq.get(sitemap[0])
            if r.status_code != 200:
                print("cannot get content of {}".format(self.get_site()['url']))
                return

            tree = etree.iterparse(StringIO(r.content))
            cur = self.__conn.cursor()
            for action, elem in tree:
                if not elem.text.strip():
                    continue
                r = rq.get(elem.text)
                if r.status_code != 200:
                    continue

                dom = html.fromstring(r.content)
                breadcrumbs = dom.xpath('//*[contains(@class, "list-breadcrumb")]//li')
                if len(breadcrumbs) < 1:
                    continue
                categories = breadcrumbs[1:]
                parent_id = 0
                for category in categories:
                    href = category.xpath('.//a')
                    name = category.xpath('.//span//text()')
                    name = name[0] if len(name) > 0 else None

                    if len(href) < 1:
                        href = elem.text
                    else:
                        href = href[0].xpath('.//@href')
                        href = href[0] if len(href) > 0 else None

                    strsql = "select id from private_categories where url = %(url)s and lang = %(lang)s"
                    res = cur.execute(strsql, {
                        "url": href,
                        "lang": sitemap[1]
                    })
                    if res < 1:
                        strsql = """insert into private_categories(parent_id,name,lang,url,site_id,create_tm)
                                    values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))
                                 """
                        cur.execute(strsql, {
                            "parent_id": parent_id,
                            "name": name,
                            "lang": sitemap[1],
                            "url": href,
                            "site_id": self.get_site()['id']
                        })
                        self.__conn.commit()
                        parent_id = cur.lastrowid
                    else:
                        cat_id = cur.fetchall()[0]
                        strsql = """update private_categories set 
                                        parent_id = %(parent_id)s,
                                        name = %(name)s,
                                        lang = %(lang)s,
                                        url = %(url)s,
                                        site_id = %(site_id)s,
                                        modify_tm = unix_timestamp(now())
                                    where id = %(id)s
                                 """
                        cur.execute(strsql, {
                            "parent_id": parent_id,
                            "name": name,
                            "lang": sitemap[1],
                            "url": href,
                            "site_id": self.get_site()['id'],
                            "id": cat_id
                        })
                        self.__conn.commit()
                        parent_id = cat_id


            cur.close()

    def collect_products(self):
        """collects products from sitemap"""
        print("shop.az started collecting products")
        sitemaps = [
            ['https://shop.az/sitemap-products-az.xml','az'],
            ['https://shop.az/sitemap-products-ru.xml','ru'],
            ['https://shop.az/sitemap-products-en.xml','en'],
        ]

        for sitemap in sitemaps:
            tree = self.get_tree(sitemap)
            for action, elem in tree:
                if not elem.text.strip():
                    continue

                cur = self.__conn.cursor()

                # check if product exists
                if self.product_exists(url=elem.text, lang=sitemap[1]):
                    print("product exists ", elem.text)
                    continue

                print("inserting product ", elem.text)

                try:
                    dom = self.get_dom(elem.text)
                except Exception as err:
                    print("DOM error:", err, elem.text)
                    continue

                try:
                    # get category id
                    category_id = self.save_category(
                        dom=dom,
                        lang=sitemap[1],
                        site_id=self.get_site()['id']
                    )
                except Exception as err:
                    print("Category error:", err, elem.text)
                    continue

                try:
                    product_id = self.save_product(
                        dom=dom,
                        url=elem.text,
                        category_id=category_id,
                        lang=sitemap[1]
                    )
                except Exception as err:
                    print("Product save error:", err, elem.text)
                    continue

                try:
                    self.save_properties(dom=dom, product_id=product_id)
                except Exception as err:
                    print("Property err:", err, elem.text)

                try:
                    self.save_images(dom=dom,product_id=product_id)
                except Exception as err:
                    print("Image error:", err, elem.text)

    def update_products(self):
        """update products"""
        print("shop.az started updating products")

        curprd = self.__conn.cursor()

        strsql = """select id, url, lang from products 
                    where site_id = %(site_id)s
                    order by scan_tm asc limit 1000"""
        curprd.execute(strsql, {
            "site_id": self.get_site()['id']
        })
        for product_id, url, lang, in curprd:

            print("updating product ", url)

            try:
                dom = self.get_dom(url)
            except Exception as err:
                print("DOM error:", err, url)
                strsql = """update products set in_stock = 0 where id = %(id)s"""
                curcat = self.__conn.cursor()
                curcat.execute(strsql, {
                    "id": product_id
                })
                self.__conn.commit()
                curcat.close()
                continue

            try:
                # get category id
                category_id = self.save_category(
                    dom=dom,
                    lang=lang,
                    site_id=self.get_site()['id']
                )
            except Exception as err:
                print("Category error:", err, url)
                continue

            try:
                product_id = self.save_product(
                    dom=dom,
                    url=url,
                    category_id=category_id,
                    lang=lang,
                    product_id=product_id
                )
            except Exception as err:
                print("Product save error:", err, url)
                continue

            try:
                self.save_properties(dom=dom, product_id=product_id)
            except Exception as err:
                print("Property err:", err, url)

            try:
                self.save_images(dom=dom,product_id=product_id)
            except Exception as err:
                print("Image error:", err, url)

    def save_properties(self, **kwargs):
        """saves properties"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        cur = self.__conn.cursor()

        props = dom.xpath('//*[contains(@class, "product___features__item")]')
        for prop in props:
            key = prop.xpath('.//*[contains(@class, "key")]//text()')
            key = key[0].strip()[:-1] if len(key) else None

            product_color = prop.xpath('.//*[@class="product__color"]//@style')
            if len(product_color):
                value = product_color[0].replace("background-color: ", "").strip()[:-1]
            else:
                value = ','.join([prop for prop in prop.xpath('.//*[contains(@class, "value")]//text()') if prop.strip()]).strip()

            if key is None or key == '' or value is None or value == '':
                continue

            strsql = "select id from properties where name = %(name)s"
            res = cur.execute(strsql, {
                "name": key
            })

            if res:
                property_id = [id for id, in cur][0]
            else:
                strsql = "insert into properties(name)values(%(name)s)"
                cur.execute(strsql, {
                    "name": key,
                })
                self.__conn.commit()
                property_id = cur.lastrowid

            strsql = """insert into product_property_rel(product_id,property_id,value)
                        values(%(product_id)s,%(property_id)s,%(value)s)
                     """
            cur.execute(strsql, {
                "product_id": kwargs.get("product_id"),
                "property_id": property_id,
                "value": value
            })
            self.__conn.commit()
        cur.close()

    def save_images(self, **kwargs):
        """saves images"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        cur = self.__conn.cursor()

        images = set()
        imgs = dom.xpath('//img[@itemprop="image"]//@src')
        for img in imgs:
            if img.startswith('http') or not img.endswith('.jpg'):
                continue
            img = self.get_site()['url'] + img
            images.add(img)

        for pos,img in enumerate(images):
            is_default = 0
            if pos == 0:
                is_default = 1

            strsql = """insert into product_images(url,product_id,is_default)
                        values(%(url)s,%(product_id)s,%(is_default)s)
                     """
            cur.execute(strsql, {
                "url": img,
                "product_id": kwargs.get("product_id"),
                "is_default": is_default
            })
            self.__conn.commit()
        cur.close()

    def save_product(self, **kwargs):
        """saves product"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        name = dom.xpath('//h1[@itemprop="name"]//text()')
        name = name[0].strip() if len(name) else None

        price = dom.xpath('//meta[@itemprop="price"]//@content')
        price = price[0] if len(price) else 0

        currency = dom.xpath('//meta[@itemprop="priceCurrency"]//@content')
        currency = currency[0].lower() if len(currency) else None

        order_url = dom.xpath('//a[contains(@class, "submit-order")]//@href')
        order_url = order_url[0] if len(order_url) else kwargs.get("url")

        description = dom.xpath('//p[@itemprop="description"]//text()')
        description = '\n'.join(description).strip() if len(description) else None

        product_id = kwargs.get("product_id")

        cur = self.__conn.cursor()

        if product_id:
            # update product
            strsql = """update products 
                        set category_id = %(category_id)s,
                        lang = %(lang)s,
                        name = %(name)s,
                        price = %(price)s,
                        description = %(description)s,
                        currency = %(currency)s,
                        scan_tm = unix_timestamp(now())
                        where id = %(id)s
                        """
            cur.execute(strsql, {
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "id": product_id
            })
        else:
            # insert product
            strsql = """insert into products(site_id,url,category_id,lang,name,price,description,currency,order_url,create_tm,scan_tm)
                        values(%(site_id)s,%(url)s,%(category_id)s,%(lang)s,%(name)s,%(price)s,%(description)s,%(currency)s,%(order_url)s,unix_timestamp(now()), unix_timestamp(now()))
                     """

            cur.execute(strsql, {
                "site_id": self.get_site()['id'],
                "url": kwargs.get("url"),
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "order_url": order_url
            })
            self.__conn.commit()
        cur.close()
        return product_id if product_id else cur.lastrowid

    def get_tree(self,sitemap):
        """returns sitemap tree"""
        r = rq.get(sitemap[0])
        if r.status_code != 200:
            print("cannot get content of {}".format(self.get_site()['url']))
            return []

        return etree.iterparse(StringIO(r.content))

    def product_exists (self, url, lang):
        """checks if product exists"""
        cur = self.__conn.cursor()
        strsql = "select id from products where url = %(url)s and lang = %(lang)s"
        res = cur.execute(strsql, {"url":url, "lang": lang})
        cur.close()
        return res > 0


    def save_category(self, **kwargs):
        """insert/select category"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is incorrect")

        category = dom.xpath('//*[@class="product__details font-noto-s-bold gray-text"]//a')
        if len(category) < 1:
            raise Exception("Category was not found")

        category_name=category[0].xpath('.//text()')[1]
        category_url=category[0].xpath('.//@href')[0]

        cur = self.__conn.cursor()

        strsql = "select id from private_categories where url=%(url)s and lang = %(lang)s"
        res = cur.execute(strsql, {
            'url': category_url,
            'lang': kwargs.get('lang')
        })

        if res < 1:
            strsql = """insert into private_categories(name,lang,url,site_id,create_tm)
                             values(%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))
                             """
            cur.execute(strsql, {
                'name': category_name,
                'lang': kwargs.get('lang'),
                'site_id': kwargs.get('site_id'),
                "url": category_url
            })
            self.__conn.commit()
            category_id = cur.lastrowid
        else:
            for category_id, in cur:
                pass

        cur.close()
        return category_id


