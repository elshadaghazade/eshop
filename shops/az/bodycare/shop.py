# coding: utf-8

from ... import parent_shop
from ... import mydb
import requests as rq
from lxml import html, etree
from StringIO import StringIO

class Shop(parent_shop.ParentShop):
    __conn = None

    def __init__(self):
        self.__conn = mydb.MyDB.get_conn()
        self.__lang = "az"

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()

    def collect_categories(self):
        pass

    def collect_products(self):
        """collects products from sitemap"""
        print("bodycare.az started collecting products")
        sitemaps = [
            ['https://www.bodycare.az/sitemap.xml']
        ]

        i = 0
        for sitemap in sitemaps:
            tree = self.get_tree(sitemap)
            for action, elem in tree:

                try:
                    lang = elem.getnext().get('hreflang')
                    url = elem.getnext().get('href')
                    if not url.endswith('.html'):
                        continue
                except Exception as err:
                    continue

                cur = self.__conn.cursor()

                # check if product exists
                if self.product_exists(url=url, lang=lang):
                    print("product exists ", url)
                    continue

                print("inserting product ", url)

                try:
                    dom = self.get_dom(url)
                except Exception as err:
                    print("DOM error:", err, url)
                    continue

                try:
                    # get category id
                    category_id = self.save_category(
                        dom=dom,
                        lang=lang,
                        site_id=self.get_site()['id']
                    )
                except Exception as err:
                    print("Category error:", err, url)
                    continue

                try:
                    # get brands
                    brand_id = self.save_brand(dom)
                except Exception as err:
                    print("Brand error:", err, url)
                    brand_id = 0

                try:
                    product_id = self.save_product(
                        dom=dom,
                        url=url,
                        category_id=category_id,
                        lang=lang,
                        brand_id=brand_id
                    )
                except Exception as err:
                    print("Product save error:", err, url)
                    continue

                try:
                    self.save_images(dom=dom,product_id=product_id)
                except Exception as err:
                    print("Image error:", err, url)

    def update_products(self):
        """update products"""

        curprd = self.__conn.cursor()

        strsql = """select id, url, lang from products 
                    where site_id = %(site_id)s
                    order by scan_tm asc limit 100"""
        curprd.execute(strsql, {
            "site_id": self.get_site()['id']
        })
        for product_id, url, lang, in curprd:

            print("updating product ", url)

            try:
                dom = self.get_dom(url)
            except Exception as err:
                print("DOM error:", err, url)
                strsql = """update products set in_stock = 0 where id = %(id)s"""
                curcat = self.__conn.cursor()
                curcat.execute(strsql, {
                    "id": product_id
                })
                self.__conn.commit()
                curcat.close()
                continue

            try:
                # get category id
                category_id = self.save_category(
                    dom=dom,
                    lang=lang,
                    site_id=self.get_site()['id']
                )
            except Exception as err:
                print("Category error:", err, url)
                continue

            try:
                # get brands
                brand_id = self.save_brand(dom)
            except Exception as err:
                print("Brand error:", err, url)
                brand_id = 0

            try:
                product_id = self.save_product(
                    dom=dom,
                    url=url,
                    category_id=category_id,
                    lang=lang,
                    brand_id=brand_id,
                    product_id=product_id
                )
            except Exception as err:
                print("Product save error:", err, url)
                continue

            try:
                self.save_images(dom=dom,product_id=product_id)
            except Exception as err:
                print("Image error:", err, url)
        curprd.close()

    def get_dom(self, url):
        """get page dom"""
        r = rq.get(url)
        if r.status_code != 200:
            raise Exception("cannot get content of {}".format(url))

        return html.fromstring(r.content)

    def save_brand(self, dom):
        """saves brands"""
        cur = self.__conn.cursor()

        brand = dom.xpath('//meta[@itemprop="brand"]//@content')
        if not len(brand):
            raise Exception("there is no brand")

        brand = brand[0].strip()
        strsql = """select id from brands where name = %(name)s"""
        res = cur.execute(strsql, {
            "name": brand
        })

        if res > 0:
            for id, in cur:
                return id

        strsql = """insert into brands(name)values(%(name)s)"""
        cur.execute(strsql, {
            "name": brand
        })

        self.__conn.commit()

        return cur.lastrowid

    def save_images(self, **kwargs):
        """saves images"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        cur = self.__conn.cursor()

        images = set()
        imgs = set(dom.xpath('//img[@class="moreview_small_thumb" or @class="moreview_thumb_image"]//@src'))

        for img in imgs:
            if img.startswith('http') or not img.endswith('.jpg'):
                continue
            img = self.get_site()['url'] + img
            images.add(img)

        for pos,img in enumerate(images):
            is_default = 1 if pos == 0 else 0

            strsql = """insert into product_images(url,product_id,is_default)
                        values(%(url)s,%(product_id)s,%(is_default)s)
                     """
            cur.execute(strsql, {
                "url": img,
                "product_id": kwargs.get("product_id"),
                "is_default": is_default
            })
            self.__conn.commit()
        cur.close()

    def save_product(self, **kwargs):
        """saves product"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is not correct")

        in_stock = 1

        name = dom.xpath('//meta[@itemprop="name"]//@content')
        name = name[0].strip() if len(name) else None

        price = dom.xpath('//meta[@itemprop="priceCurrency" and @content="AZN"]/preceding::meta[1]')
        if not len(price):
            raise Exception("Product price error")

        price = price[0].xpath('.//@content')
        price = price[0] if len(price) else 0

        currency = 'azn'
        order_url = kwargs.get("url")

        description = dom.xpath('//meta[@itemprop="description"]//@content')
        description = '\n'.join(description).strip() if len(description) else None

        product_id = kwargs.get("product_id")

        cur = self.__conn.cursor()

        if product_id:
            # update product
            strsql = """update products 
                        set in_stock = %(in_stock)s,
                        category_id = %(category_id)s,
                        lang = %(lang)s,
                        name = %(name)s,
                        brand_id = %(brand_id)s,
                        price = %(price)s,
                        description = %(description)s,
                        currency = %(currency)s,
                        scan_tm = unix_timestamp(now())
                        where id = %(id)s
                        """
            cur.execute(strsql, {
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "in_stock": in_stock,
                "brand_id": kwargs.get("brand_id"),
                "id": product_id
            })
        else:
            #insert product
            strsql = """insert into products(in_stock,site_id,url,category_id,lang,name,brand_id,price,description,currency,order_url,create_tm,scan_tm)
                        values(%(in_stock)s,%(site_id)s,%(url)s,%(category_id)s,%(lang)s,%(name)s,%(brand_id)s,%(price)s,%(description)s,%(currency)s,%(order_url)s,unix_timestamp(now()), unix_timestamp(now()))
                     """

            cur.execute(strsql, {
                "site_id": self.get_site()['id'],
                "url": kwargs.get("url"),
                "category_id": kwargs.get("category_id"),
                "lang": kwargs.get("lang"),
                "name": name,
                "price": price,
                "description": description,
                "currency": currency,
                "order_url": order_url,
                "in_stock": in_stock,
                "brand_id": kwargs.get("brand_id")
            })
        self.__conn.commit()
        cur.close()
        return product_id if product_id else cur.lastrowid

    def get_tree(self,sitemap):
        """returns sitemap tree"""
        r = rq.get(sitemap[0])
        if r.status_code != 200:
            print("cannot get content of {}".format(self.get_site()['url']))
            return []

        return etree.iterparse(StringIO(r.content))

    def product_exists (self, url, lang):
        """checks if product exists"""
        cur = self.__conn.cursor()
        strsql = "select id from products where url = %(url)s and lang = %(lang)s"
        res = cur.execute(strsql, {"url":url, "lang": lang})
        cur.close()
        return res > 0


    def save_category(self, **kwargs):
        """insert/select category"""
        dom = kwargs.get("dom")
        if dom is None:
            raise Exception("DOM is incorrect")

        categories = dom.xpath('//a[@class="middle-crumb" or @class="last-crumb"]')

        if len(categories) < 1:
            raise Exception("Category was not found")

        category_id = 0
        for category in categories[1:]:
            category_name = category.xpath('.//text()')
            category_name = category_name[0].strip() if len(category_name) and category_name[0].strip() else None

            category_url = category.xpath('.//@href')
            category_url = self.get_site()['url'] +  category_url[0] if len(category_url) else None

            cur = self.__conn.cursor()

            strsql = "select id from private_categories where url=%(url)s and lang = %(lang)s"
            res = cur.execute(strsql, {
                'url': category_url,
                'lang': kwargs.get('lang')
            })

            if res < 1:
                strsql = """insert into private_categories(parent_id,name,lang,url,site_id,create_tm)
                            values(%(parent_id)s,%(name)s,%(lang)s,%(url)s,%(site_id)s,unix_timestamp(now()))
                         """
                cur.execute(strsql, {
                    'name': category_name,
                    'lang': kwargs.get('lang'),
                    'site_id': kwargs.get('site_id'),
                    "url": category_url,
                    "parent_id": category_id
                })
                self.__conn.commit()
                category_id = cur.lastrowid
            else:
                for category_id, in cur:
                    pass

            cur.close()
        return category_id


