from sites import Sites
from utils.product_comparator import Comparator
import importlib as imp

class Actions:


    def __init__(self):
        pass

    def collect_categories(self, site_id=None):
        sites = Sites()

        for site in sites.get_sites(site_id):
            shop = imp.import_module(site['namespace'])
            shop = shop.Shop()
            shop.set_site(site)
            shop.collect_categories()

    def collect_products(self, site_id=None):
        sites = Sites()

        for site in sites.get_sites(site_id):
            shop = imp.import_module(site['namespace'])
            shop = shop.Shop()
            shop.set_site(site)
            shop.collect_products()

    def update_products(self, site_id=None):
        sites = Sites()

        for site in sites.get_sites(site_id):
            shop = imp.import_module(site['namespace'])
            shop = shop.Shop()
            shop.set_site(site)
            shop.update_products()

    def compare_products(self, site_id=None):
        cmp = Comparator()
        cmp.compare_products()